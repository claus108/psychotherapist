import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  headerClass = '';
  titleClass = '';
  route = '';

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((r: any) => {
      this.route = r['url'];
      this.setClass(this.route);
    });

  }

  setClass(p: any) {
    switch(p) {
      case '/services':this.headerClass = "header-service" ; this.titleClass = "title-service"; break;
      case '/about'  : this.headerClass = "header-about"; this.titleClass = "title-about"; break;
      case '/fees'   : this.headerClass = "header-fees"; this.titleClass = "title-fees"; break;
      case '/contact': this.headerClass = "header-contact"; this.titleClass = "title-contact"; break;
      case '/home' : this.headerClass = ''; this.titleClass = '';
    }
  }


}
