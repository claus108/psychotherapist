import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes }   from '@angular/router';
import { FormsModule }            from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ServicesComponent } from './services/services.component';
import { AboutComponent } from './about/about.component';
import { FeesComponent } from './fees/fees.component';
import { ContactComponent } from './contact/contact.component';
import { CopyrightComponent } from './copyright/copyright.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { SitemapComponent } from './sitemap/sitemap.component';

const appRoutes: Routes = [
  { path: 'home',                    component: HomeComponent },
  { path: 'contact',                    component: ContactComponent },
  { path: 'services',                component: ServicesComponent },
  { path: 'about',                   component: AboutComponent },
  { path: 'fees',                    component: FeesComponent },
  { path: 'legal',                 component: CopyrightComponent },
  { path: 'privacy',                 component: PrivacyComponent },
  { path: 'sitemap',                 component: SitemapComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '**',                      component: HomeComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    SidebarComponent,
    ServicesComponent,
    AboutComponent,
    FeesComponent,
    ContactComponent,
    CopyrightComponent,
    PrivacyComponent,
    SitemapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
