import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  show: boolean = false;

  events = [
    {show:false,"title":"Nulla nisl.","date":"7/18/2017","time":"3:39 PM","location":"65455 Northridge Hill","content":"Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.","city":"Bourg-en-Bresse"},
    {show:false,"title":"Duis consequat dui nec nisi volutpat eleifend.","date":"2/26/2017","time":"1:27 AM","location":"5 Hagan Park","content":"Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.","city":"Yangzhuang"},
    {show:false,"title":"Sed vel enim sit amet nunc viverra dapibus.","date":"10/10/2016","time":"5:53 PM","location":"52993 Clove Place","content":"Phasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.","city":"Jinrongjie"},
    {show:false,"title":"Praesent id massa id nisl venenatis lacinia.","date":"2/4/2017","time":"10:44 PM","location":"5 Homewood Point","content":"Fusce consequat. Nulla nisl. Nunc nisl.","city":"Lewolere"},
    {show:false,"title":"Morbi vel lectus in quam fringilla rhoncus.","date":"11/2/2016","time":"5:45 AM","location":"5626 Loftsgordon Point","content":"Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.","city":"Buenavista"}]

  constructor() { }

  ngOnInit() {
  }

  showEvent(index: any) {
    this.events[index].show = !this.events[index].show;
  }

}
